//
//  SceneDelegate.h
//  Hypo
//
//  Created by Alisher Shermatov on 1/19/22.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

